# -*- coding: utf-8 -*-
"""Helper functions
"""
import pandas as pd
import numpy as np
import matplotlib as mpl
import seaborn as sns
import matplotlib.pyplot as plt
import os
from helpers.constants import *


def read_autolab_file(file):
    """Return a dataframe with the data from the file"""
    return pd.read_csv(file, delimiter="\t")


def get_files(path, filename_string, filename_rule):
    """Return a list of files that contain given string from a given directory.
    Filename rule determines whether the string is at the beginning or the end of the file.
            """
    files = []
    with os.scandir(path) as all_files:
        for file in all_files:
            if filename_rule == 'starts':
                if file.name.startswith(filename_string):
                    files.append(file)

            if filename_rule == 'ends':
                if file.name.endswith(filename_string):
                    files.append(file)
    return files


def load_from_files(files, header_rows=0):
    """Load data from a list of files and returns list of dataframes
            """
    data_frames = []
    for file in files:
        data_from_csv = read_autolab_file(file)
        # append data from this file to the data frames list
        data_frames.append(data_from_csv)

    return data_frames


def calculate_cell_constant(frequency, measured_resistance, measured_reactance, relative_permittivity, conductivity):
    """Calculates and returns a cell constant from data measured on a reference liquid with known values.
            """
    cell_constant = np.abs((measured_resistance + 1j * measured_reactance)) * \
                    np.abs((conductivity / 10) + 1j * 2 * np.pi * frequency * EPSILON0 * relative_permittivity)

    return 1 / cell_constant


def get_property(dataframes, property_header):
    """Return a property from a dat frame defined by the header and the mean of that property across the samples.
            """
    property_ = []
    for i in range(len(dataframes)):
        property_.append(dataframes[i][property_header])
    mean = pd.concat(property_, axis=1).mean(axis=1)

    return property_, mean


def plot_sample_property(data, property_header, frequency_header, labels=[]):
    """Return a plot of given property.
            """
    frequency, frequency_mean = get_property(data, frequency_header)
    property_, property_mean = get_property(data, property_header)

    #  mean = pd.concat(property_, axis = 1).mean(axis = 1)

    fig, ax = plt.subplots(figsize=(8, 5))
    ax.set(xscale="log", yscale="linear")
    plt.grid(True, which="both")
    # ax.set_xlim([1e-1, 5e6])
    # ax.set_ylim([0.1, 10])

    for i in range(len(property_)):
        ax.plot(frequency_mean, property_[i])

    ax.plot(frequency_mean, property_mean, 'k')

    ax.set_ylabel(property_header)
    ax.set_xlabel(frequency_header)

    ax.legend(labels)

    return fig, ax


def calculate_conductivity(k, resistance, reactance):
    """Calculates and returns the conductivity from resistance and cell constant values. Multiplied by 10 to get mS/cm
            """
    conductivity = []
    for i in range(len(resistance)):
        conductivity.append(10 * (resistance[i] / (resistance[i] ** 2 + reactance[i] ** 2)) / k)
        # conductivity.append(1 / (resistance[i] * k) * 10)
    return conductivity


def plot_mean_and_std(x_data, mean_data, std, colors):

    fig, ax = plt.subplots(figsize=(8, 5))
    ax.set(xscale="log", yscale="linear")
    plt.grid(True, which="both")

    for i in range(len(mean_data)):

        #plt.plot(x_data[i], mean_data[i], 'o', color=colors[i])

        #plt.plot(x_data[i], mean_data[i], '-', color='gray', label='_nolegend_')

        plt.plot(x_data[i], mean_data[i], '-', color=colors[i])

        plt.fill_between(x_data[i], mean_data[i] - std[i], mean_data[i] + std[i],
                         color=colors[i], alpha=0.2)

    return fig, ax

